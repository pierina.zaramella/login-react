import axios from 'axios'
const url = 'http://localhost:3000/characters'

const getCharactersList = async(token) => {
    return axios.get(url, {headers: {Authorization: 'Bearer '+ token}})
}

export default getCharactersList