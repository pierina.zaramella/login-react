import axios from 'axios'
const url = 'http://localhost:3000/users/'

const userRegister = async (user) => {
    return axios.put(`${url}register`, {...user}, {headers: {'Content-Type':'application/json'}})
}

const userLogin = async (user) => {
    return axios.post(`${url}login`, {...user}, {headers: {'Content-Type':'application/json'}})
}

const userLogout = async (username, token) => {
    return axios.post(`${url}logout`, {username}, {headers: {Authorization: 'Bearer '+ token}})
}

const userAuthenticated = async (token) => {
    return axios.post(`${url}authenticated`, {}, {headers: {Authorization: 'Bearer '+ token}})
}

export {userRegister, userLogin, userLogout, userAuthenticated}
