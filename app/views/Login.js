import React, {Component, createContext} from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { withRouter } from 'react-router-dom';

import Snackbar from '../components/Snackbar';
import { userLogin } from '../services/Users'

class Login extends Component{
   state = {
      loading: false,
      snackbar: {
          visible: false,
          message: '',
          type: 'success'
      },
      user: {username: '', password: ''},
  } 

   onLogin = async () => {
    if(this.state.user.username && this.state.user.password){
      try{
        const response = await userLogin(this.state.user)
        console.log(response.data)
        if(response.status === 200){
          localStorage.setItem('token', response.data.token)
          localStorage.setItem('username', this.state.user.username)
          this.props.history.push("/registered", {token:response.data.token})
        } 
      } catch(e){
        this.setState(state => ({
          ...state,
          snackbar: {visible: true, 
                    message: 'Favor verificar usuario y password!', 
                    type:  'error'  }              
        }))
      }
    }
  }

  updateUserProps = (e) => {
    console.log(this.state)
    const {id, value} = e.target
    this.setState(state => ({
        ...state,
        user: {...state.user, [id]:value}
    })) 
  }

  handleCloseSnackbar = () => {
    this.setState(state => ({
            ...state,
            snackbar: {...state.snackbar, visible: false}
        })
    )
  }

  render(){
    const classes = this.props
    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              autoComplete="username"
              autoFocus
              onKeyUp={this.updateUserProps}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              onKeyUp={this.updateUserProps}
            />
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={this.onLogin}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item>
                <Link href="/register" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
        </div>
        <Snackbar open={this.state.snackbar.visible} message={this.state.snackbar.message} type={this.state.snackbar.type} handleClose={this.handleCloseSnackbar}></Snackbar>
      </Container>    
    );
  }
  
}

export default withRouter(Login)