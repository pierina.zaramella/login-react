import React, {Component} from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Loading from '../components/Loading'

import getCharactersList from '../services/Characters'
import NavBar from '../components/NavBar'
import { withRouter } from 'react-router-dom';

class Characters extends Component{
    state = {
        list: [],
        loading: false
    }
    

    async componentDidMount(){
        const token = localStorage.getItem('token')

        if(token){
            try{
            this.setState({loading: true})
            const response = await getCharactersList(token)
            console.log(response.data)
             this.setState({
                list: response.data
            }) 
            } catch(e){
                this.props.history.push('/')
            }
            this.setState({loading: false})    
        }
                       
        else
            this.props.history.push('/')
    }

    render(){
        console.log(this.state.list)
        const items = this.state.list.map((i, index) => {
                return (
                    <>
                    <ListItem alignItems="flex-start">
                        <ListItemAvatar>
                            <Avatar alt={i.name} src={i.image} />
                        </ListItemAvatar>
                        <ListItemText
                            primary={i.name}
                            secondary={
                                <>
                                    <Typography
                                        component="span"
                                        variant="body2"
                                        color="textPrimary"
                                    >
                                    {i.species} <br></br>
                                    </Typography>
                                    {'Gender: ' + i.gender + '. Status: ' + i.status + '.'} 
                                </>
                        }
                        ></ListItemText>
                    </ListItem>
                    <Divider variant="inset" component="li" />
                    </>)
            })

        return(
            <>
                <NavBar></NavBar>
                { this.state.loading && <Loading></Loading>} 
                <List component="nav" aria-label="main mailbox folders" id="list">
                    {items}
                </List>
            </>
                )
            }
}

export default withRouter(Characters)
