import React, {Component} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { withRouter } from 'react-router-dom';

import { userRegister } from '../services/Users'
import Snackbar from '../components/Snackbar';

class Register extends Component{

state={
    user: { name: "", lastname: "", username: "", password: "" } ,
    passwordConfirm: '',
    snackbar: {
        visible: false,
        message: '',
        type: 'success'
    }
}

onRegister = async() => {
    console.log(this.state.user)
    try{
        if(this.state.passwordConfirm === this.state.user.password){
            const register = await userRegister(this.state.user)
            console.log(register)
        } else {
            this.setState(state => ({
                ...state,
                snackbar: {visible: true, 
                          message: 'El password no coincide!', 
                          type:  'error'  }              
              }))
        }
    } catch(e) {
        this.setState(state => ({
            ...state,
            snackbar: {visible: true, 
                      message: 'Error al registrar el usuario!', 
                      type:  'error'  }              
          }))
    }
        
}

updateUserProps = (e) => {
    console.log(this.state)
    const {id, value} = e.target
    if(e.target.id != 'passwordConfirm')
        this.setState(state => ({
            ...state,
            user: {...state.user, [id]:value}
        })) 
    else 
        this.setState(state => ({
            ...state,
            passwordConfirm: value
        })) 
}

handleCloseSnackbar = () => {
    this.setState(state => ({
            ...state,
            snackbar: {...state.snackbar, visible: false}
        })
    )
  }

render(){
    const classes = this.props
    return (
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
            <Typography component="h1" variant="h5">
            Sign up
            </Typography>
            <Grid container spacing={2} style={{marginTop: "10px"}}>
                <Grid item xs={12} sm={6}>
                <TextField
                    autoComplete="name"
                    name="name"
                    variant="outlined"
                    required
                    fullWidth
                    id="name"
                    label="Name"
                    onKeyUp={this.updateUserProps}
                    autoFocus
                />
                </Grid>
                <Grid item xs={12} sm={6}>
                <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="lastname"
                    label="Last Name"
                    name="lastname"
                    autoComplete="lname"
                    onKeyUp={this.updateUserProps}
                />
                </Grid>
                <Grid item xs={12}>
                <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="username"
                    label="username"
                    name="username"
                    autoComplete="username"
                    onKeyUp={this.updateUserProps}
                />
                </Grid>
                <Grid item xs={12}>
                <TextField
                    variant="outlined"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    onKeyUp={this.updateUserProps}
                />
                </Grid>
                <Grid item xs={12}>
                <TextField
                    variant="outlined"
                    required
                    fullWidth
                    name="passwordConfirm"
                    label="Password confirm"
                    type="password"
                    id="passwordConfirm"
                    autoComplete="confirm-password"
                    onKeyUp={this.updateUserProps}
                />
                </Grid>
            </Grid>
            <Button
                fullWidth
                variant="contained"
                color="primary"
                style={{marginTop: "20px"}}
                onClick={this.onRegister}
            >
                Sign Up
            </Button>
            <Grid container justify="flex-end">
                <Grid item style={{marginTop: "10px"}}>
                <Link href="/" variant="body2">
                    Already have an account? Sign in
                </Link>
                </Grid>
            </Grid>
        </div>
        <Snackbar open={this.state.snackbar.visible} message={this.state.snackbar.message} type={this.state.snackbar.type} handleClose={this.handleCloseSnackbar}></Snackbar>
        </Container>
    );
    }
}

export default withRouter(Register)