import React, {Component} from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import NavBar from '../components/NavBar'

import {userAuthenticated} from '../services/Users'
import { withRouter } from 'react-router-dom';
import { Divider } from '@material-ui/core';

class Registered extends Component{
    state = {
        list: []
    }

    async componentDidMount(){
        console.log(this.props)
        const token = localStorage.getItem('token')

        if(token)
                try{
                    const response = await userAuthenticated(token)
                    console.log(response.data)
                    this.setState({
                        list: response.data
                    })
                } catch(e){
                    this.props.history.push('/')
                }           
        else
            this.props.history.push('/')
    }

    render(){
        const items = this.state.list.map((i, index) => {
                return (
                    <>
                    <ListItem>
                        <ListItemText
                            primary={'Nombre: ' + i.name + ' ' + i.lastname}
                            secondary={'Usuario: '+ i.username}
                        ></ListItemText>
                    </ListItem>
                    <Divider></Divider>
                    </>)
            })

        return(
            <>
                <NavBar></NavBar>
                <List component="nav" aria-label="main mailbox folders" id="list">
                    {items}
                </List>
            </>)
            }
}

export default withRouter(Registered)
