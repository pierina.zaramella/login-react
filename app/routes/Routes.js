import React, { Component } from 'react'
import {BrowserRouter, Route} from 'react-router-dom'
import ErrorBoundary from '../views/ErrorBoundary'
import Register from '../views/Register'
import Login from '../views/Login'
import Registered from '../views/Registered'
import Characters from '../views/Characters'

/* Routes */
/* Nav, autentification, error bundle */
export default class Routes extends Component {
  
    render(){
        return (
            <ErrorBoundary>
              <BrowserRouter>
                <Route exact path="/" component={Login} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/registered" component={Registered} />
                <Route exact path="/list" component={Characters} />
              </BrowserRouter>
            </ErrorBoundary>
          )
    }
}
