import React, {Component} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router-dom';
import { userLogout } from '../services/Users';

class NavBar extends Component {
  onLogout = () => {
      userLogout(localStorage.getItem('username'), localStorage.getItem('token'))
      localStorage.removeItem('username')
      localStorage.removeItem('token')
      this.props.history.push('/')
  }

  gotoPage = (page) => () => {
      console.log(this.props)
    this.props.history.push(page)
  }
  render(){
    const classes = this.props
    return (
            <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" style={{flexGrow: 1}}>
                        Rick and Morthy
                    </Typography>
                    <Button color="inherit" onClick={this.gotoPage('/registered')}>Active Users</Button>
                    <Button color="inherit" onClick={this.gotoPage('/list')}>Rick and Morthy Characters</Button>
                    <Button color="inherit" onClick={this.onLogout}>Logout</Button>
                </Toolbar>
            </AppBar>
            </div>
        );
    }
}

export default withRouter(NavBar)