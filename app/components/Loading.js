import React, {Component} from 'react'
import '../css/loading.css'

import CircularProgress from '@material-ui/core/CircularProgress'


export default class Loading extends Component {
    render(){
        return (
            <div className="containerProgress">
                <CircularProgress color="secondary"/>
            </div>
        )
    }
}